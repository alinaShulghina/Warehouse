package com.softserve.edu.warehouse.dao;

import com.softserve.edu.warehouse.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by alin- on 27.12.2017.
 */
@RunWith(Parameterized.class)
public class DaoTest {

    class Student implements Serializable {

        private String name;
        private int age;
        private Main.Animal animal;

        public Student() {
        }

        public Student(String name, int age, Main.Animal animal) {
            this.name = name;
            this.age = age;
            this.animal = animal;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Main.Animal getAnimal() {
            return animal;
        }

        public void setAnimal(Main.Animal animal) {
            this.animal = animal;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", animal=" + animal +
                    '}';
        }
    }

    class Animal implements Serializable {
        private String name;

        public Animal() {
        }

        public Animal(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Parameter(0)
    public Student student;
    @Parameter(1)
    public int m2;
    @Parameter(2)
    public int result;


    // creates the test data
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { 1 , 2, 2 },
                { 5, 3, 15 },
                { 121, 4, 484 } };
        return Arrays.asList(data);
    }



}