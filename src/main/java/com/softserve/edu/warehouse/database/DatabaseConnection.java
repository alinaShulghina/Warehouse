package com.softserve.edu.warehouse.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by alin- on 01.01.2018.
 */
public class DatabaseConnection {

    private static Connection connection;

    public static Connection getConnection(String databaseUrl, String username, String password) {
        try {
            connection = DriverManager.getConnection(databaseUrl, username, password);
        } catch (SQLException e) {
            System.out.println("Unable to open connection!");
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Unable to close connection!");
        }
    }

    public static Connection getConnection() {
        return connection;
    }
}
