package com.softserve.edu.warehouse.database;

import java.lang.reflect.Field;

import java.sql.*;
import java.util.Map;


/**
 * Created by alin- on 27.12.2017.
 */
public class DatabaseQueries {

    public static boolean createTable(Connection connection, Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        StringBuilder columns = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            columns.append(fields[i].getName());
            columns.append(" ");
            if (mapFieldType(fields[i].getType().getSimpleName()).equals("BINARY(255)")) {
                createTable(connection, fields[i].getType());
                columns.append("INTEGER NOT NULL REFERENCES ");
                columns.append(fields[i].getType().getSimpleName());
                columns.append("(ID)");
            } else {
                columns.append(mapFieldType(fields[i].getType().getSimpleName()));
            }
            if (i != fields.length - 1) columns.append(",\n");
        }
        try {
            PreparedStatement statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS "
                    + clazz.getSimpleName().toLowerCase() + " (" +
                    "ID INT AUTO_INCREMENT PRIMARY KEY NOT NULL," + columns + " )");
            System.out.println(statement);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Unable to execute query");
        }
        return true;
    }


    public static long insertObject(Connection connection, Class<?> clazz, Object object) {
        long insertedObjectId = 0;
        try {
            Field[] fields = clazz.getDeclaredFields();
            String query = prepareInsertStatement(clazz);
            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int index = 1;
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);
                if (mapFieldType(field.getType().getSimpleName()).equals("BINARY(255)")) {
                    long foreignKey = insertObject(connection, field.getType(), field.get(object));
                    preparedStatement.setObject(index++, foreignKey);
                } else preparedStatement.setObject(index++, field.get(object));
            }
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedObjectId = rs.getLong(1);
            }
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return insertedObjectId;
    }

    public static boolean deleteObject(Connection connection, Class clazz, int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM " +
                    clazz.getSimpleName() + " WHERE ID  = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static Map<Integer, Object> findAll(Connection connection, Class clazz) throws Exception {
        ResultSet resultSet = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + clazz.getSimpleName());
            resultSet = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return CustomObjectMapper.mapObject(resultSet, clazz);
    }

    public static Object findById(Connection connection, long id, Class clazz) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + clazz.getSimpleName()
                + " WHERE ID = ?");
        preparedStatement.setLong(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        return CustomObjectMapper.mapObject(resultSet, clazz).get(0);
    }

    public static String mapFieldType(String type) {
        String mySqlType = "BINARY(255)";
        if (type.equals("Integer") || type.equals("int")) {
            mySqlType = "INTEGER";
        }
        if (type.equalsIgnoreCase("boolean")) {
            mySqlType = "BIT(1)";
        }
        if (type.equals("Character") || type.equals("char")) {
            mySqlType = "CHAR(50)";
        }
        if (type.equalsIgnoreCase("double")) {
            mySqlType = "DOUBLE";
        }
        if (type.equalsIgnoreCase("float")) {
            mySqlType = "FLOAT";
        }
        if (type.equalsIgnoreCase("byte")) {
            mySqlType = "SMALLINT";
        }
        if (type.equalsIgnoreCase("long")) {
            mySqlType = "BIGINT";
        }
        if (type.equals("String")) {
            mySqlType = "VARCHAR(50)";
        }
        return mySqlType;
    }

    private static String prepareInsertStatement(Class<?> clazz) throws SQLException {
        String query;
        Field[] fields = clazz.getDeclaredFields();
        StringBuilder fieldsNames = new StringBuilder();
        StringBuilder values = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            fieldsNames.append(field.getName());
            values.append("?");
            if (i != fields.length - 1) {
                fieldsNames.append(",");
                values.append(",");
            }
        }
        query = "INSERT INTO " + clazz.getSimpleName() +
                " (" + fieldsNames + ") " + " VALUES (" + values + ")";
        return query;
    }

}
