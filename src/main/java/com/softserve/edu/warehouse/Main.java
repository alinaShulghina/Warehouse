package com.softserve.edu.warehouse;

import com.softserve.edu.warehouse.dao.Dao;
import com.softserve.edu.warehouse.dao.DaoFactory;
import com.softserve.edu.warehouse.database.DatabaseConnection;
import com.softserve.edu.warehouse.database.DatabaseQueries;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alin- on 25.12.2017.
 */
@SuppressWarnings("unchecked")
public class Main {
    public static void main(String[] args) {
        Connection connection = DatabaseConnection.getConnection("jdbc:mysql://localhost:3306/demo?useSSL=false",
                "root", "student");
        try {
            Student student = (Student) DatabaseQueries.findById(connection, 3, Student.class);
            System.out.println(student.getName());
            System.out.println(student.getAge());
            System.out.println(student.getAnimal().getName());
        } catch (Exception e){
            System.out.println("smth is wrong");
        }
        DatabaseConnection.closeConnection();
    }

    public static class Student {

        private String name;
        private int age;
        private Animal animal;

        public Student() {
        }

        public Student(String name, int age, Animal animal) {
            this.name = name;
            this.age = age;
            this.animal = animal;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Animal getAnimal() {
            return animal;
        }

        public void setAnimal(Animal animal) {
            this.animal = animal;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", animal=" + animal +
                    '}';
        }
    }

    public static class Animal {
        private String name;

        public Animal() {
        }

        public Animal(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
