package com.softserve.edu.warehouse.dao;

import java.util.List;
import java.util.Map;


/**
 * Created by alin- on 25.12.2017.
 */
public interface Dao<T> {
    Map<Long,T> findAll();

    T findById(long id);

    void delete(long id);

    long save(T object); //returns the id of saved object

    long[] saveAll(List<T> objects); //returns ids of saved objects

    void update(long id, T newObject);
}
