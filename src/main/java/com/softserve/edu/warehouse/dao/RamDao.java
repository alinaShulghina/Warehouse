package com.softserve.edu.warehouse.dao;

import java.util.*;

/**
 * Created by alin- on 25.12.2017.
 */
@SuppressWarnings("unchecked")
public class RamDao<T> implements Dao<T> {

    private Map<Long, T> objects = new HashMap<>();

    private static long id = 1;

    @Override
    public Map findAll() {
        return objects;
    }

    @Override
    public T findById(long id) {
        return objects.get(id);
    }

    @Override
    public void delete(long id) {
        objects.remove(id);
    }

    @Override
    public long save(T object) {
        long newObjectId = id++;
        objects.put(newObjectId, object);
        return newObjectId;
    }

    @Override
    public long[] saveAll(List objects) {
        List<T> list = objects;
        long[] ids = new long[list.size()];
        for (int i = 0; i < ids.length; i++) {
            long newObjectId = id++;
            this.objects.put(newObjectId, list.get(i));
            ids[i] = newObjectId;
        }
        return ids;
    }

    @Override
    public void update(long id, T newObject) {
        objects.replace(id, newObject);
    }
}
