package com.softserve.edu.warehouse.dao;


import java.io.*;
import java.util.*;

/**
 * Created by alin- on 26.12.2017.
 */
//@SuppressWarnings("unchecked")
public class BinaryDao<T extends Serializable> implements Dao<T> {

    private Map<Long,T> objects = new HashMap<>();

    private File file; //file, which stores serialized objects

    private File idFile; //file, which stores last used id ob object

    private static final String CLASS_NAME = "BINARY";

    @Override
    public Map<Long, T> findAll() {
        return null;
    }

    @Override
    public T findById(long id) {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public long save(T object) {
        try(ObjectOutputStream objectOutputStream = getOutputStream()){
            objectOutputStream.writeObject(object);
        }catch (IOException e){
            System.out.println("Unable to write object to file!");
        }
        return 0;
    }

    @Override
    public long[] saveAll(List<T> objects) {
        return new long[0];
    }

    @Override
    public void update(long id, T newObject) {

    }

    private ObjectOutputStream getOutputStream() throws IOException {
        if (file.exists()) {
            return new AppendableObjectOutputStream(new FileOutputStream(file, true));
        } else {
            return new ObjectOutputStream(new FileOutputStream(file));
        }
    }

    private class AppendableObjectOutputStream extends ObjectOutputStream {

        public AppendableObjectOutputStream(OutputStream out) throws IOException {
            super(out);
        }

        @Override
        protected void writeStreamHeader() throws IOException {
            // do not write a header
        }
    }


}
