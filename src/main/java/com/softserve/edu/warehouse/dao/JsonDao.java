package com.softserve.edu.warehouse.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.impl.MapEntrySerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by alin- on 26.12.2017.
 */
public class JsonDao<T> implements Dao<T> {


    @Override
    public Map<Long, T> findAll() {
        return null;
    }

    @Override
    public T findById(long id) {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public long save(T object) {
        return 0;
    }

    @Override
    public long[] saveAll(List<T> objects) {
        return new long[0];
    }

    @Override
    public void update(long id, T newObject) {

    }
}
