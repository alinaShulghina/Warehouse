package com.softserve.edu.warehouse.dao;

import com.softserve.edu.warehouse.database.DatabaseQueries;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * Created by alin- on 26.12.2017.
 */

public class DatabaseDao<T> implements Dao<T> {

    private Class<T> clazz;

    private Connection connection;

    DatabaseDao(Class<T> clazz, Connection connection) {
        this.clazz = clazz;
        this.connection = connection;
    }

    @Override
    public Map<Long, T> findAll() {
        return null;
    }

    @Override
    public T findById(long id) {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public long save(T object) {
        DatabaseQueries.createTable(connection, clazz);
        DatabaseQueries.insertObject(connection, clazz, object);
        return 0;
    }

    @Override
    public long[] saveAll(List<T> objects) {
        return new long[0];
    }

    @Override
    public void update(long id, T newObject) {

    }
}
