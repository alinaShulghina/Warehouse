package com.softserve.edu.warehouse.dao;

import java.io.Serializable;

/**
 * Created by alin- on 26.12.2017.
 */
public class DaoFactory {
    public static final int BINARY = 1;
    public static final int JSON = 2;
    public static final int RAM = 3;
    public static final int XML = 4;
    public static final int DATABASE = 5;

    public static Dao getDao(int daoId, Class<?> clazz) {
        Dao dao = null;
//        if (daoId == BINARY) {
//            dao = new BinaryDao<>(clazz);
//        }
//        if (daoId == JSON) {
//            dao = new JsonDao<>(clazz);
//        }
        if (daoId == DATABASE) {
//            dao = new DatabaseDao<>(clazz);
        }
        if (daoId == RAM) {
            dao = new RamDao<>();
        }
        if (daoId == XML){
            dao = new XmlDao<>();
        }
        return dao;
    }
}
